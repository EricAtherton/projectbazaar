<?php

use Illuminate\Database\Seeder;

class IdeasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the table of any existing data.
        DB::table('ideas')->truncate();

        //Create an array of project titles 
        $titles = [
        	'Module handbook creation tool', 'Project Bazaar', 'E-Commerce Online Store', 'School register web application', 'Online daily planner web application', 'UX design', 'Design of large scale web application', 'Gym planner', 'Security issue of Cloud based computing', 'Bug tracking system'
        ];

        //Store a lorem ipsum paragraph inside a variable for use when inserting the new record.
        $description = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        //Set current date to variable
        $date = new DateTime();

        //Loop over the project titles creating a new record each time using the title set, description set, a random owner (1-10), random likes (0-50) and then set timestamps.
        foreach($titles as $key => $title) {
        	DB::table('ideas')->insert([
        		'title' => $title,
        		'description' => $description,
        		'owner' => rand(1, 10),
        		'likes' => rand(0, 50),
                'category_id' => rand(1, 6),
        		'created_at' => $date,
        		'updated_at' => $date
        	]);
        }
    }
}
