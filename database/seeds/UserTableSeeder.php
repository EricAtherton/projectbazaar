<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the table of any existing data.
        DB::table('users')->truncate();

        //Use factory method to create 10 random users.
        factory('App\User', 10)->create();

        //Set current date to variable
        $date = new DateTime();

        //Create four users within the database for testing and marking purposes. 
        DB::table('users')->insert([
            'name' => 'Eric Atherton',
            'email' => 'eric.atherton@go.edgehill.ac.uk',
            'password' => bcrypt('password'),
            'accesslevel' => 3,
            'profileimage' => '/images/userdefault.png',
            'remember_token' => str_random(10),
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('users')->insert([
            'name' => 'Admin Account',
            'email' => 'admin@go.edgehill.ac.uk',
            'password' => bcrypt('admin123'),
            'accesslevel' => 3,
            'profileimage' => '/images/userdefault.png',
            'remember_token' => str_random(10),
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('users')->insert([
            'name' => 'Staff Account',
            'email' => 'staff@go.edgehill.ac.uk',
            'password' => bcrypt('staff123'),
            'accesslevel' => 2,
            'profileimage' => '/images/userdefault.png',
            'remember_token' => str_random(10),
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('users')->insert([
            'name' => 'Student Account',
            'email' => 'student@go.edgehill.ac.uk',
            'password' => bcrypt('student123'),
            'accesslevel' => 1,
            'profileimage' => '/images/userdefault.png',
            'remember_token' => str_random(10),
            'created_at' => $date,
            'updated_at' => $date
        ]);
    }
}
