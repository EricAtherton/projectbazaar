<?php

use Illuminate\Database\Seeder;

class ExamplesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the table of any existing data.
        DB::table('examples')->truncate();

        //Create an array of project id's.
        $projects = [1,2,3,4,5,6,7,8,9,10];

        //Set current date to variable
        $date = new DateTime();

        foreach($projects as $project) {

        	//Set random number between 1 and 3.
        	$grade = rand(1, 3);

        	//Set appropriate text based on grade value.
        	if($grade == '1') {
        		$grade = 'First';
        	} else if ($grade == '2') {
        		$grade = '2:1';
        	} else {
        		$grade = '2:2';
        	}

            //Insert a new record using the fields set.
        	DB::table('examples')->insert([
        		'project_id' => $project,
        		'grade' => $grade,
        		'created_at' => $date,
        		'updated_at' => $date
        	]);
        }


    }
}
