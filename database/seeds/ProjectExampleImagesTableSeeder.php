<?php

use Illuminate\Database\Seeder;

class ProjectExampleImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the table of any existing data.
        DB::table('example_images')->truncate();

        //Create an array of project id's.
        $examples = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

        //Set current date to variable
        $date = new DateTime();

        //Place holder image path
        $path = '/images/placeholder.png';

        //Loop over the examples creating a new record each time using the information set.
        foreach($examples as $example) {
        	DB::table('example_images')->insert([
        		'path' => $path,
        		'example_id' => $example,
        		'created_at' => $date,
        		'updated_at' => $date
        	]);
        }
    }
}
