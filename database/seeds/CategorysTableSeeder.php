<?php

use Illuminate\Database\Seeder;

class CategorysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Clear the table of any existing data.
        DB::table('categorys')->truncate();

        //Create an array to loop over with the diffrent categories.
        $categorys = ['Web Development', 'UX Design', 'PHP', 'JavaScript', 'Computer forensics', 'Databases'];

        //Set current date to variable
        $date = new DateTime();

        //Loop over the categories creating a new record each time using the category and add timestamps.
        foreach($categorys as $category) {
        	DB::table('categorys')->insert([
        		'category' => $category,
        		'created_at' => $date,
        		'updated_at' => $date
        	]);
        }
    }
}