<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    //Run all the diffrent seeder files when the db:seed function is ran from the command line.
    public function run()
    {

    	$this->call(UserTableSeeder::class);
    	$this->call(IdeasTableSeeder::class);
    	$this->call(ExamplesTableSeeder::class);
    	$this->call(CategorysTableSeeder::class);
        $this->call(ProjectExampleImagesTableSeeder::class);

    }
}
