<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    //Create the users table with the fields: id, name, email, password, accesslevel and profile image. 
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('accesslevel');
            $table->string('profileimage');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    //Drop the users table.
    public function down()
    {
        Schema::drop('users');
    }
}
