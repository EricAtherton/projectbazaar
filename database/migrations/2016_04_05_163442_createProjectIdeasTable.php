<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectIdeasTable extends Migration
{
    //Create the projectIdeas table with the fields: id, title, description, owner, likes and category_id 
    public function up()
    {
        Schema::create('ideas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('owner')->unsigned();
            $table->integer('likes');
            $table->integer('category_id')->unsigned();
            $table->timestamps();
        });
    }

    //Drop projectIdeas table.
    public function down()
    {
        Schema::drop('ideas');
    }
}
