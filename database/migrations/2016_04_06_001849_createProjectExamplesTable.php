<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectExamplesTable extends Migration
{
    //Create the projectExamples table with the fields: id, project_id, grade. 
    public function up()
    {
        Schema::create('examples', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->string('grade');
            $table->timestamps();
        });
    }

    //Drop the projectExamples table.
    public function down()
    {
        Schema::drop('examples');
    }
}