<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExampleImagesTable extends Migration
{
    //Create the example_images table with the fields: id, path, example_id. 
    public function up()
    {
        Schema::create('example_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->integer('example_id')->unsigned();
            $table->timestamps();
        });
    }

    //Drop example_images table.
    public function down()
    {
        Schema::drop('example_images');
    }
}
