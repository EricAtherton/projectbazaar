<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    //Use the ideas table within the database.
    protected $table = 'ideas';

    //Allow the title, description, likes, category_id and owner fields to be filled with data.
    protected $fillable = [
    	'title', 'description', 'likes', 'category_id', 'owner'
    ];

    //Create a relationship to the category attached to the project ideas so it can be displayed within the view.
    public function category()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }

    //Create a relationship with the diffrent project examples attached to the project idea so they can be accessed within the view.
    public function examples()
    {
    	return $this->hasMany('App\Example', 'project_id');
    }

    //Create a relationship with the user that owns the project idea so this can be accessed within the view.
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'owner');
    }
}
