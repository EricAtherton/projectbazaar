<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectExampleImages extends Model
{
	//Use the ideas table within the database.
    protected $table = 'example_images';

    //Allow the path and example_id field to be filled with data. 
    protected $fillable = [
    	'path', 'example_id'
    ];
}
