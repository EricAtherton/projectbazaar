<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	//Use the categorys table within the database.
    protected $table = 'categorys';

    //Allow the category field to be filled with data.
    protected $fillable = [
    	'category'
    ];
}
