<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Example extends Model
{
    //Use the examples table within the database.
    protected $table = 'examples';

    //Allow the project_id and grade fields to be filled with data.
    protected $fillable = [
    	'project_id', 'grade'
    ];

    /* Create a relationship between the project examples and the project example images that are attached to this project example so they can be accessed in the view and displayed to the user. */
    public function images()
    {
    	return $this->hasMany('App\ProjectExampleImages', 'example_id', 'id');
    }

    //Create a relationship between the project example and the project idea it belongs to. This will allow the idea to be accessed from within the view.
    public function idea()
    {
    	return $this->hasOne('App\Idea', 'id', 'project_id');
    }
}
