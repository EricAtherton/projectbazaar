<?php

namespace App\Http\Controllers;

//Load requirements for the controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Idea;
use App\User;
use App\Category;
use Auth;

class IdeasController extends Controller
{
    //Check to see if the user is logged in or not.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* When the default (/projectIdeas) or project ideas list route is requisted return the project ideas overview with all the project ideas stored in the database so they can be displayed in the application. Return the logged in user for use within the view and categoryies for use within the filter form. */
    public function index()
    {
        $user = User::findORFail(Auth::user()->id);
        $ideas = Idea::all();
        $categorys = Category::lists('category', 'id');
        return view('ideas.list', ['ideas' => $ideas])->with(['user' => $user])->with(['categorys' => $categorys]);
    }

    /* Used to return the show view for a project idea. First find the logged in user and then find the appropriate idea based on the ID provided and return these to the show view */
    public function show($id)
    {
        $user = User::findORFail(Auth::user()->id);
        $idea = Idea::findORFail($id);
        return view('ideas.show', ['idea' => $idea])->with(['user' => $user]);
    }

    /* Used to return the edit function. First find the logged in user then find the idea based on the ID provided. Then return a list of all the categoryies and also their id's for used within the form. Return the edit view with the relevant idea, logged in user and categories. */
    public function edit($id)
    {
        $user = User::findORFail(Auth::user()->id);
        $idea = Idea::findORFail($id);
        $categorys = Category::lists('category', 'id');
        return view('ideas.edit', ['idea' => $idea])->with(['user' => $user])->with(['categorys' => $categorys]);
    }

    /* Used to update the project idea within the database. First find the relevant idea within the database using the id provided. Then update this idea using the information provided by the user. Finally redirect the user to the project show page for the updated project idea. */
    public function update(Requests\UpdateIdeaRequest $request, $id)
    {
        $idea = Idea::findORFail($id);

        $idea->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'category_id' => $request->get('category')
        ]);

        return \Redirect::route('projectIdeas.show', $id)->with('message', $request->get('title') . ' has been successfully updated!');
    }

    /* Return the create view for the user. First find the logged in user within the database, then return a list of all the categories names and also id's. Finally return the project idea create view with the logged in user and categories for use within the form. */
    public function create()
    {
        $user = User::findORFail(Auth::user()->id);
        $categorys = Category::lists('category', 'id');

        return view('ideas.create', ['user' => $user])->with(['categorys' => $categorys]);
    }

    /* Used for storing the new project idea within the database. First create a new instance of the idea model and store this in a variable, then find the logged in user and store this within a variable. Add the information gathered from the user to the idea model, setting the owner to the retrieved user and the likes to 0. Save the idea and redirect the user to the project ideas overview page with a message to let them know their action was successful */
    public function store(Requests\CreateIdeaRequest $request)
    {
        $idea = new Idea;
        $user = Auth::user()->id;

        $idea->title = $request->title;
        $idea->description = $request->description;
        $idea->owner = $user;
        $idea->likes = 0;
        $idea->category_id = $request->category;

        $idea->save();

        return \Redirect::route('projectIdeas.index')->with('message', ' has been successfully created!'); 
    }

    /* Used to delete a project idea from the database. Find the idea using the id provided and destroy this record, returning the user to the project ideas overview page with a message to let them know their action was successful. */
    public function destroy($id)
    {
        Idea::destroy($id);

        return \Redirect::route('projectIdeas.index')->with('message', 'has been successfully deleted!'); 
    }
}
