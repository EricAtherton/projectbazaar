<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //Check to see if the user is logged in or not.
    public function __construct()
    {
        $this->middleware('auth');
    }

    //When the default route (/home) is requested return the home view.
    public function index()
    {
        return view('home');
    }
}
