<?php

namespace App\Http\Controllers;

//Load requirements for controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    //Check to see if the user is logged in.
    public function __construct()
    {
        $this->middleware('auth');
    }

    //When the default (/users) or users index route is requested return the users overview page with all the users and also the logged in user.
    public function index()
    {
        $loggedUser = User::findORFail(Auth::user()->id);
        $users = User::all();
        return view('users.list', ['users' => $users])->with(['loggedUser' => $loggedUser]);
    }

    //Return the create user interface with the logged in user.
    public function create()
    {
        $loggedUser = User::findORFail(Auth::user()->id);
        return view('users.create', ['loggedUser' => $loggedUser]);
    }

    /* Used to store new user within the database. First create a new instance of the user model and then check to see if they have uploaded a profile picture. If they have then move uploaded file to the correct folder and set a variable for the file name so it can be loaded within the views, if they have not uploaded a profile picture then set a default one. Fill the user model with the information provided by the user and also the file path. Save the new user to the database and redirect the user to the users overview page with a message to let them know their action was successful. */
    public function store(Requests\CreateUserRequest $request)
    {
        $user = new User;

        if($request->profileimage != null){
            $file = array('profileimage' => $request->image);
            $path = 'images/';
            $fileName = $request->profileimage->getClientOriginalName();
            $fullFilePath = '/images/' . $fileName;

            $request->profileimage->move($path, $fileName);
        } else {
            $fullFilePath = '/images/userdefault.png';
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->accesslevel = $request->accesslevel;
        $user->profileimage = $fullFilePath;

        $user->save();

        return \Redirect::route('users.index')->with('message', $request->get('title') . 'User has been successfully created!');
    }

    /* Find the user within the database using the id provided and destroy that record. Redirect the user to the users overview page with a message letting them know their action was successful. */
    public function destroy($id)
    {
        User::destroy($id);

        return \Redirect::route('users.index')->with('message', 'User has been successfully deleted!');
    }

    //Return the user edit interface for the user with the user found based on the id provided and also the logged in user.
    public function edit($id)
    {
        $loggedUser = User::findORFail(Auth::user()->id);
        $user = User::findORFail($id);
        return view('users.edit', ['user' => $user])->with(['loggedUser' => $loggedUser]);
    }

    /* Used for updating a user within the database. First find the user within the database using the id provided and store this result within a variable. Check to see if an image has been uploaded and then repeat the process described in the edit function. Check to see if the access level field is set (only admins will be able to access this option but students and staff also use this form) if it then set the provided information to the accesslevel variable. If it is not then set the variable to the users current access level. Update the user record with all the new information and redirect them to the home page with a message letting them know that the account has been successfully updated. */
    public function update(Requests\UpdateUserRequest $request, $id)
    {
        $user = User::findORFail($id);

        if($request->profileimage != null){
            $file = array('profileimage' => $request->profileimage);
            $path = 'images/';
            $fileName = $request->profileimage->getClientOriginalName();
            $fullFilePath = '/images/' . $fileName;

            $request->profileimage->move($path, $fileName);
        } else {
            $fullFilePath = $user->profileimage;
        }

        if(isset($request->accesslevel)){
            $accesslevel = $request->get('accesslevel');
        } else {
            $accesslevel = $user->accesslevel;
        }

        $user->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'accesslevel' => $accesslevel,
            'password' => bcrypt($request->password),
            'profileimage' => $fullFilePath
        ]);

        return \Redirect::route('home.index')->with('message', 'Account has been successfully updated!'); 
    }

    /* Return the logged in user and also the appropriate user from the database based on the id provided with the users show view. */
    public function show($id) 
    {
        $loggedUser = User::findORFail(Auth::user()->id);
        $user = User::findORFail($id);
        return view('users.show', ['user' => $user])->with(['loggedUser' => $loggedUser]);
    }
}