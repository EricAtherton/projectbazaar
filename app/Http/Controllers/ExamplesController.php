<?php

namespace App\Http\Controllers;

//Load requirements for the controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Idea;
use App\User;
use App\ProjectExampleImages;
use App\Example;
use Auth;


class ExamplesController extends Controller
{
    //Check to see if the user is logged in or not
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* When the default route (/projectExamples) or list view is requested return the overview of all the project examples. All examples are sent to the view along with the logged in user. */
    public function index()
    {
        $user = User::findORFail(Auth::user()->id);
        $examples = Example::all(); 
        return view('examples.list', ['examples' => $examples])->with(['user' => $user]);
    }

    //Used to return the edit interface to the user. The logged in user is sent with the example that is being edited for use within the view.
    public function edit($id)
    {
        $user = User::findORFail(Auth::user()->id);
        $example = Example::findORFail($id);
        return view('examples.edit', ['example' => $example])->with(['user' => $user]);
    }

    /* Used to update the project example within the database. First find the relevant example with the id provided from the request, then update it with the new grade gathered from the user. Check to see if an image has been uploaded, if it has then create a new project example images model to store the new image within the project example images table. Get the id from the example previously found within the database and then store the file path using the default route (/images/) and also the file name to create a working file path to the image which can be loaded in the view. Move the image to the correct folder and save the new project example images model. Return the user to the project examples overview page with a message to let them know their actions was successful. */
    public function update(Requests\UpdateExampleRequest $request, $id)
    {
        $example = Example::findORFail($id);

        $example->update([
            'grade' => $request->get('grade')
        ]);

        if($request->image != null){
            $exampleImage = new ProjectExampleImages;
            $exampleImage->example_id = $id;
            $file = array('image' => $request->image);

            $path = 'images/';
            $fileName = $request->image->getClientOriginalName();
            $fullFilePath = '/images/' . $fileName;

            $request->image->move($path, $fileName);

            $exampleImage->path = $fullFilePath;

            $exampleImage->save();

            return \Redirect::route('projectExamples.index')->with('message', ' has been successfully updated!');
        }

        return \Redirect::route('projectExamples.index')->with('message', ' has been successfully updated!');    
    }

    /* Used for returning the interface for adding a new project example. Include the logged in user and a list of only the project ideas title and id within a variable for use in the view. */
    public function create()
    {
        $user = User::findORFail(Auth::user()->id);
        $ideas = Idea::lists('title', 'id');

        return view('examples.create', ['user' => $user])->with(['ideas' => $ideas]);
    }

    /* Used to store the new project example within the database. First create a new instance of an example model. Then get the information from the user and use it to fill the model and finally save the new record to the database. Create a new instance of the project example images model to store the example that is attached to the project idea. Get the example id by using the example previously saved then use the default image path along with the file name to create a working file path for use within the view to display the image. Move the image to the correct folder and save the new project example images model. Return the user to the project examples overview page with a message to let them know their action was successful */
    public function store(Requests\CreateExampleRequest $request)
    {
        $example = new Example;

        $example->project_id = $request->idea;
        $example->grade = $request->grade;

        $example->save();

        $exampleImage = new ProjectExampleImages;
        
        $exampleImage->example_id = $example->id;
        $file = array('image' => $request->image);
        $path = 'images/';
        $fileName = $request->image->getClientOriginalName();
        $fullFilePath = '/images/' . $fileName;

        $request->image->move($path, $fileName);

        $exampleImage->path = $fullFilePath;

        $exampleImage->save();

        return \Redirect::route('projectExamples.index')->with('message', ' has been successfully submitted!');   
    }

    /* Find the appropriate example within the database using the id provided and destroy the record. Return the user to the project examples overview page with a message to let them know their action was successful. */
    public function destroy($id)
    {
        Example::destroy($id);

        return \Redirect::route('projectExamples.index')->with('message', 'has been successfully deleted!'); 
    }
}
