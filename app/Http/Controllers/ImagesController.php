<?php

namespace App\Http\Controllers;

//Load requirements for controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\ProjectExampleImages;

class ImagesController extends Controller
{
    //Check to see if the user is logged in.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Used to destroy a project example image. First find the project example image within the database using the id provided and then destroy that record. Redirect the user to the project examples overview page with a message to let know their action was successful. */
    public function destroy($id)
    {
        ProjectExampleImages::destroy($id);

        return \Redirect::route('projectExamples.index')->with('message', ' image has been successfully deleted!'); 
    }
}