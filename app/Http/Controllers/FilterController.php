<?php

namespace App\Http\Controllers;

//Load requirements for controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Idea;
use App\User;
use App\Category;
use Auth;


class FilterController extends Controller
{
    //Check to see if the user is logged in.
    public function __construct()
    {
        $this->middleware('auth');
    }

    //When the default link (/filter) is loaded redirect them to the project ideas overview page.
    public function index()
    {
        return \Redirect::route('projectIdeas.index'); 
    }

    /* Used to return the project ideas overview but only with the project ideas related to the category specified by the user. Select the project ideas from the database if their category matches the one submitted by the user and store these in a variable. Return the project ideas overview page with these refined ideas, the logged in user and also the categories for use within the filter form.  */
    public function store(Requests\FilterRequest $request)
    {
        $user = User::findORFail(Auth::user()->id);
        $ideas = Idea::where('category_id', $request->category)->get();
        $categorys = Category::lists('category', 'id');
        return view('ideas.list', ['ideas' => $ideas])->with(['user' => $user])->with(['categorys' => $categorys]);
    }
}