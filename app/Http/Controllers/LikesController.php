<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Idea;
use Auth;

class LikesController extends Controller
{
    //Check to see if the user if logged in.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Called when the like button is clicked. First find the relevant idea within the database using the id provided and store it within a variabl. Extract the likes from the project idea and increment it once then update the project idea with this new value finally redirect the used to the project show page for the relevant project idea. */
    public function edit($id)
    {
        $idea = Idea::findORFail($id);

        $likes = $idea->likes;
        $likes++;

        $idea->update([
            'likes' => $likes
        ]);

        return \Redirect::route('projectIdeas.show', $id);

    }
}