<?php

namespace App\Http\Controllers;

//Load in requirements for controller.
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Category;
use Auth;

class CategorysController extends Controller
{
    //Check to see if the user is logged in or not
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Used for returning the categorys create view with the logged in user stored in a variable. 
    public function create()
    {
        $user = User::findORFail(Auth::user()->id);

        return view('categorys.create', ['user' => $user]);
    }

    /* Used to store the new category when the user submits the form. First create a variable with a new instance of the category model then get the input from the user to 
    store within the new model. Save the model to the database and return the user to the project ideas overview page with a message to let them know there action was
    successful. */
    public function store(Requests\CreateCategoryRequest $request)
    {
        $category = new Category;

        $category->category = $request->category;

        $category->save();

        return \Redirect::route('projectIdeas.index')->with('message', $request->get('title') . ' category has successfully been created, please now add your project idea.');
    }
}