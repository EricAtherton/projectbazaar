<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Auth route
Route::auth();

// Attach routes to the appropriate controllers.
Route::get('/', 'HomeController@index');
Route::resource('/home', 'HomeController');
Route::resource('/projectIdeas', 'IdeasController');
Route::resource('/projectExamples', 'ExamplesController');
Route::resource('/images', 'ImagesController');
Route::resource('/users', 'UsersController');
Route::resource('/likes', 'LikesController');
Route::resource('/filter', 'FilterController');
Route::resource('/categorys', 'CategorysController');