<?php 
$I = new FunctionalTester($scenario);

$I->am('a admin');
$I->wantTo('perform actions and see result');

//When 
$I->amOnPage('/');

//Then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel 5', '.title');
