<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Edit user @endsection

@section('content')

<!-- Create form for editing a user profile. -->
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Update account</div>
                <div class="panel-body">
                	{!! Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'put', 'files' => true)) !!}
                		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                			{!! Form::label('name', 'Name:', array('class' => 'formLabel')) !!}
                			{!! Form::text('name', $user->name, array('class' => 'form-control')) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                			{!! Form::label('email', 'Email Address:', array('class' => 'formLabel')) !!}
                			{!! Form::text('email', $user->email, array('class' => 'form-control',)) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                		</div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::label('password', 'New password:', array('class' => 'formLabel')) !!}
                            {!! Form::password('password', array('class' => 'form-control')) !!}
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            {!! Form::label('password_confirmation', 'Confirm password:', array('class' => 'formLabel')) !!}
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- If the user is an admin allow them to edit the access level -->
                        @if($loggedUser->accesslevel == 3)
                    		<div class="form-group">
                    			{!! Form::label('accesslevel', 'Access level:', array('class' => 'formLabel')) !!}
                    			{!! Form::select('accesslevel', array('3' => 'Admin', '2' => 'Tutor', '1' => 'Student'), $user->accesslevel, array('class' => 'form-control')) !!}
                    		</div>
                        @endif
                        <div class="form-group{{ $errors->has('profileimage') ? ' has-error' : '' }}">
                            {!! Form::label('profileimage', 'Update profile image:', array('class' => 'formLabel')) !!}
                            {!! Form::file('profileimage', array('class' => 'form-control')) !!}
                            @if ($errors->has('profileimage'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('profileimage') }}</strong>
                                </span>
                            @endif
                        </div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Update', array('class' => 'btn btn-success btn-block')) !!}
	                			<div class="cancelButton"><a href="{{ url('/users') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection