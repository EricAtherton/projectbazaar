<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Create new user @endsection

@section('content')
<div class="container">

    <!-- Check that the user is an admin if they are not inform them that they do not have access to this page. -->
	@if($loggedUser->accesslevel != 3)
		<p class="lead">You don't have access to this page.</p>
	@else 
    <!-- Create form for adding a new user to the database. The form will post to the users controller where it will be handeled. -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Create user</div>
                <div class="panel-body">
                	{!! Form::open(array('route' => 'users.store', 'files' => true)) !!}
                		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                			{!! Form::label('name', 'Name:', array('class' => 'formLabel')) !!}
                			{!! Form::text('name', 'Name...', array('class' => 'form-control')) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                			{!! Form::label('email', 'Email Address:', array('class' => 'formLabel')) !!}
                			{!! Form::text('email', 'Email...', array('class' => 'form-control',)) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                		</div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::label('password', 'Password:', array('class' => 'formLabel')) !!}
                            {!! Form::password('password', array('class' => 'form-control')) !!}
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            {!! Form::label('password_confirmation', 'Confirm password:', array('class' => 'formLabel')) !!}
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                		<div class="form-group">
                			{!! Form::label('accesslevel', 'Access level:', array('class' => 'formLabel')) !!}
                			{!! Form::select('accesslevel', array('3' => 'Admin', '2' => 'Tutor', '1' => 'Student'), null, array('class' => 'form-control')) !!}
                		</div>
                        <div class="form-group{{ $errors->has('profileimage') ? ' has-error' : '' }}">
                            {!! Form::label('profileimage', 'Add profile image:', array('class' => 'formLabel')) !!}
                            {!! Form::file('profileimage', array('class' => 'form-control')) !!}
                            @if ($errors->has('profileimage'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('profileimage') }}</strong>
                                </span>
                            @endif
                        </div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Submit', array('class' => 'btn btn-success btn-block')) !!}
	                			<div class="cancelButton"><a href="{{ url('/users') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection