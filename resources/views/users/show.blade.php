<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') {{ $user->name }} @endsection

@section('content')

<div class="container">
	<!-- Present profile image, name and email. If the user is an admin then show edit and delete options. -->
	<div class="row">
		<div class="col-md-3">
			<img src="{{ $user->profileimage }}" class="img-rounded exampleEditImages">
		</div>
		<div class="col-md-6">
			<h1>{{ $user->name }}</h1>
			<p class="lead"><strong>Email: </strong>{{ $user->email }}</p>
			<!-- Show type of user -->
			<p class="lead">
				<strong>
					@if($user->accesslevel == 3)
						Admin
					@elseif($user->accesslevel == 2)
						Staff 
					@else 
						Student
					@endif
				</strong>
			</p>
		</div>
		<div class="col-md-3">
			<div class="ideaShowButtons">
			@if($loggedUser->accesslevel == 3)
				<a href="{{ route('users.edit', [$user->id]) }}" class="btn btn-success"><i class="fa fa-btn fa-edit"></i>Update</a></td>
				{!! Form::model($user, array('route' => array('users.destroy', $user->id), 'method' => 'delete', 'class' => 'deleteButton', 'onsubmit' => 'return ConfirmDelete()')) !!}
					{!! Form::hidden('_method', 'DELETE') !!}
			        {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
				{!! Form::close() !!}	
			@endif
		</div>
	</div>
	<!-- If the user has submitted any project ideas then present them here in a table format so students can see which are the staff specalizes in. -->
	@if($user->ideas->count())
	<div class="row">
		<div class="col-md-12">
			<h3 class="usersProjects">Project ideas submitted by this user:</h3>
			<table class="table table-hover">
				<tr>
					<th>Project title</th>
					<th>Category</th>
					<th>Last updated</th>
				</tr>
				@foreach($user->ideas as $idea)
					<tr>
						<td><a href="{{ route('projectIdeas.show', [$idea->id]) }}" class="normalLinks">{{ $idea->title }}</a></td>
						<td>{{ $idea->category->category }}</td>
						<td>{{ date('d, F, Y', strtotime($idea->updated_at)) }}</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
	@endif
</div>

@endsection