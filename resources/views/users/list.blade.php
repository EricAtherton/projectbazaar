<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Project Ideas @endsection

@section('content')

<div class="container">
	<!-- If a message has been set display it to the user. -->
	@if(Session::get('message'))
	    <div class="alert alert-success">
	        <strong>{{ Session::get('message') }}</strong>
	    </div>
	@endif
	<!-- If the user is a student then display all the staff profiles. -->
	@if($loggedUser->accesslevel == 1)
		<div class="row">
			<div class="col-md-12">
				<h1>Members of staff</h1>
			</div>
		</div>
		<table class="table table-hover">
			<tr>
				<th>Profile image</th>
				<th>Name</th>
				<th>Email</th>
			</tr>
			@foreach($users as $user)
				@if($user->accesslevel == 2)
					<tr>
						<td><img src="{{ $user->profileimage }}" class="profileImage img-circle"></td>
						<td><a href="{{ route('users.show', [$user->id]) }}" class="normalLinks">{{ $user->name }}</a></td>
						<td>{{ $user->email }}</td>
					</tr>
				@endif
			@endforeach
		</table>
	<!-- If the user is a admin or a member of staff show all the profiles that have been created. Add a button for admins for adding a new account and add an account for all users for upading their own account. Present all of these user profiles in a tables adding extra columns for edit and delete options if the user is an admin. -->
	@else
		<div class="row">
			<div class="col-md-12">
				<h1>All users</h1>
				<div class="usersTopButtons">
					@if($loggedUser->accesslevel == 3)
						<a href="{{ route('users.create') }}" class="btn btn-success"><i class="fa fa-btn fa-plus-circle"></i>Create new user</a>
					@endif
					<a href="{{ route('users.edit', [$loggedUser->id]) }}" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i>Update my account</a>
				</div>
			</div>
		</div>
		<table class="table table-hover">
			<tr>
				<th>Profile image</th>
				<th>Name</th>
				<th>Email</th>
				@if($loggedUser->accesslevel == 3)
					<th>Access level</th>
					<th></th>
					<th></th>
				@endif
			</tr>
			@foreach($users as $user)
				<tr>
					<td><img src="{{ $user->profileimage }}" class="profileImage img-circle"></td>
					<td><a href="{{ route('users.show', [$user->id]) }}" class="normalLinks">{{ $user->name }}</a></td>
					<td>{{ $user->email }}</td>
					@if($loggedUser->accesslevel == 3)
						<td>{{ $user->accesslevel }}</td>
						<td><a href="{{ route('users.edit', [$user->id]) }}" class="btn btn-success"><i class="fa fa-btn fa-edit"></i>Update</a></td>
						<td>
							{!! Form::model($user, array('route' => array('users.destroy', $user->id), 'method' => 'delete', 'class' => 'deleteButton', 'onsubmit' => 'return ConfirmDelete()')) !!}
								{!! Form::hidden('_method', 'DELETE') !!}
		                    	{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
							{!! Form::close() !!}
						</td>
					@endif
				</tr>
			@endforeach
		</table>
	@endif
</div>

@endsection