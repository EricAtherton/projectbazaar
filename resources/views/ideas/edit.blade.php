<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Login @endsection

@section('content')
<div class="container">

    <!-- If the user is a student then dont allow them to see the edit project example form and return message to let them know they dont have access -->
	@if($user->accesslevel == 1)
		<p class="lead">You don't have access to this page.</p>
	@else
    <!-- Create a form for editing a project idea styled with Bootstrap CSS. Form wil post to the project ideas controller where it will be handeled. --> 
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $idea->title }}</div>
                <div class="panel-body">
                	{!! Form::model($idea, array('route' => array('projectIdeas.update', $idea->id), 'method' => 'put')) !!}
                		<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                			{!! Form::label('title', 'Project Title:', array('class' => 'formLabel')) !!}
                			{!! Form::text('title', $idea->title, array('class' => 'form-control')) !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                			{!! Form::label('description', 'Description:', array('class' => 'formLabel')) !!}
                			{!! Form::textarea('description', $idea->description, array('class' => 'form-control', 'row' => '5')) !!}
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group">
                			{!! Form::label('category', 'Category:', array('class' => 'formLabel')) !!}
                			{!! Form::select('category', $categorys, $idea->category->category, array('class' => 'form-control')) !!}
                		</div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Update project', array('class' => 'btn btn-success btn-block')) !!}
                                <!-- Add a cancel button for the user if they require it, they will be redirected to the project ideas overview page -->
	                			<div class="cancelButton"><a href="{{ url('/projectIdeas') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection