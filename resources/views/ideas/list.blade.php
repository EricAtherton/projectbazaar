<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Project Ideas @endsection

@section('content')

<div class="container">
	<!-- If a message has been set display it to the user. -->
	@if(Session::get('message'))
	    <div class="alert alert-success">
	        <strong>Project idea: </strong>{{ Session::get('message') }}
	    </div>
	@endif
	<!-- Introduce the user to the page. -->
	<div class="row">
	    <div class="col-md-12">
	        <h1>Project Ideas</h1>
	        <p class="lead">This page will present project ideas that members of staff have approved and submitted to this web application. This will allow for you to browse through multiple different project suggestions so that you can begin to get an understanding of what type of project you would like to undertake for your final year project. If you see a project you like then click the project title to view more information about that given project. If you would like to see the member of staff’s profile to see other projects they have submitted then please click the name located in the created by column. If you see a project you like then express your interest and click the like button!</p>
	    </div>
	</div>
	<!-- Create a filter function so the users can see project only by specified category. Create a button for adding a project idea if the user if staff or an admin and add a button for all users for viewing all project examples. -->
	<div class="row">
		<div class="col-md-6 filterByCategory">
			<div class="filterByCategory">
				{!! Form::open(array('route' => 'filter.store', 'class' => 'form-inline')) !!}
					{!! Form::label('category', 'Filter by category:', array('class' => 'formLabel')) !!}
	                {!! Form::select('category', $categorys, null, array('class' => 'form-control')) !!}
	                {!! Form::submit('Apply', array('class' => 'btn btn-success')) !!}
				{!! Form::close() !!}
			</div>
		</div>
		<div class="col-md-6">
			<p>
				@if($user->accesslevel != 1)
					<a href="{{ route('projectIdeas.create') }}" class="btn btn-success"><i class="fa fa-btn fa-plus-circle"></i>Add new project idea</a>
				@endif
				<a href="{{ url('/projectExamples') }}" class="btn btn-primary">View all project examples</a>
			</p>
		</div>
	</div>
	<!-- Create a table to display all the project ideas and add edit and delete function for admins. If there is no content set them inform the user. -->
	<table class="table table-hover">
		@if(count($ideas) >= 1)
			<tr>
				<th>Project title</th>
				<th>Category</th>
				<th>Last updated</th>
				<th>Submitted by</th>
				<th>Likes</th>
				@if($user->accesslevel == 3)
					<th></th>
					<th></th>
				@endif
			</tr>
			@foreach($ideas as $idea)	
				<tr>
					<td><a href="{{ route('projectIdeas.show', [$idea->id]) }}" class="normalLinks">{{ $idea->title }}</a></td>
					<td>{{ $idea->category->category }}</td>
					<td>{{ date('d, F, Y', strtotime($idea->updated_at)) }}</td>
					<td><a href="{{ route('users.show', [$idea->user->id]) }}" class="normalLinks">{{ $idea->user->name }}</a></td>
					<td>{{ $idea->likes }}</td>
					@if($user->accesslevel == 3)
						<td><a href="{{ route('projectIdeas.edit', [$idea->id]) }}" class="btn btn-success"><i class="fa fa-btn fa-edit"></i>Edit</a></td>
						<td>
							{!! Form::model($idea, array('route' => array('projectIdeas.destroy', $idea->id), 'method' => 'delete', 'onsubmit' => 'return ConfirmDelete()')) !!}
								{!! Form::hidden('_method', 'DELETE') !!}
	                    		{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
							{!! Form::close() !!}
	                    </td>
					@endif
				</tr>
			@endforeach
		@else
			<tr>
				<td><h3>There is no projects for that category please select a diffrent one or click the Project Ideas link to view all.</h3></td>
			</tr>
		@endif
	</table>
</div>

@endsection