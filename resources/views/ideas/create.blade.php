<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Login @endsection

@section('content')
<div class="container">

    <!-- If the user is a student then dont allow them to see the create project example form and return message to let them know they dont have access -->
	@if($user->accesslevel == 1)
		<p class="lead">You don't have access to this page.</p>
	@else
    <!-- Create a form for adding a new project idea styled with Bootstrap CSS. Form wil post to the project ideas controller where it will be handeled. -->  
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Submit your project idea:</div>
                <div class="panel-body">
                	{!! Form::open(array('url' => 'projectIdeas')) !!}
                		<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                			{!! Form::label('title', 'Project Title:', array('class' => 'formLabel')) !!}
                			{!! Form::text('title', 'Project title...', array('class' => 'form-control')) !!}
                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                			{!! Form::label('description', 'Description:', array('class' => 'formLabel')) !!}
                			{!! Form::textarea('description', 'Project description...', array('class' => 'form-control', 'row' => '5')) !!}
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group">
                			{!! Form::label('category', 'Category:', array('class' => 'formLabel')) !!}
                			{!! Form::select('category', $categorys, null, array('class' => 'form-control')) !!}
                            <p class="formHelp"><small><mark>> Cant find the category your looking for? <a href="{{ route('categorys.create') }}" class="normalLinks">Click here to make a new one</a></mark></small></p>
                		</div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Create project', array('class' => 'btn btn-success btn-block')) !!}
                                <!-- Add a cancel button for the user if they require it, they will be redirected to the project ideas overview page -->
	                			<div class="cancelButton"><a href="{{ url('/projectIdeas') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection