<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') {{ $idea->title }} @endsection

@section('content')

<div class="container">
	<!-- If there is a message set then display it to the user. -->
	@if(Session::get('message'))
	    <div class="alert alert-success">
	        <strong>Project idea: </strong>{{ Session::get('message') }}
	    </div>
	@endif
	<!-- Use title to create page title followed by category, submitted by and last updated information. Format the date so it it more user friendly. -->
	<div class="row">
	    <div class="col-md-9">
	    	<h1>{{ $idea->title }}</h1>
			<p class="lead"><strong>Category: </strong>{{ $idea->category->category }}</p>
	    	<p class="lead"><strong>Submitted by: </strong><a href="{{ route('users.show', [$idea->user->id]) }}" class="normalLinks">{{ $idea->user->name }}</a></p>
	    	<p class="lead"><strong>Last updated: </strong>{{ date('d, F, Y', strtotime($idea->updated_at)) }}</p>
		</div>
		<div class="col-md-3">
			<!-- If the user owns the project or they are an admin let them have an edit and delete option. -->
			<div class="ideaShowButtons">
				@if($user->accesslevel == 1)	
					
				@elseif($idea->owner == $user->id || $user->accesslevel == 3)
					<a href="{{ route('projectIdeas.edit', [$idea->id]) }}" class="btn btn-success"><i class="fa fa-btn fa-edit"></i>Edit</a>
					{!! Form::model($idea, array('route' => array('projectIdeas.destroy', $idea->id), 'method' => 'delete', 'class' => 'deleteButton', 'onsubmit' => 'return ConfirmDelete()', )) !!}
						{!! Form::hidden('_method', 'DELETE') !!}
	                    {!! Form::submit('Delete', array('class' => 'btn btn-danger', 'id' => 'delete')) !!}
					{!! Form::close() !!}	
				@endif
			</div>
		</div>
	</div>
	<!-- Present the project description and the example attached to the project idea. If there is not any project examples assigned then inform the user. -->
	<div class="row">
		<div class="col-md-6">
			<h3>Description</h3>
			<p>{{ $idea->description }}</p>
		</div>
		<div class="col-md-6">
			<h3>Example</h3>
			@if ( !$idea->examples->count() )
				<p class="lead">There is currently no examples linked to this project.</p>
       	 	@else
       	 		@foreach($idea->examples as $example)
       	 			@foreach($example->images as $image)
						<img src="{{ $image->path }}" class="projectExample">
					@endforeach
					<p class="lead"><strong>Grade achieved for example above: </strong>{{ $example->grade }}</p>
				@endforeach
			@endif
		</div>
	</div>
	<!-- Add a likes button so users can show their interest in a project. -->
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route('likes.edit', [$idea->id]) }}" class="btn btn-info"><i class="fa fa-btn fa-thumbs-up"></i>{{ $idea->likes }}</a>
		</div>
	</div>
</div>

@endsection