<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Home @endsection

@section('content')
<div class="container">

    <!-- If a message has been set display it to the user. -->
    @if(Session::get('message'))
        <div class="alert alert-success">
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif

    <!-- Welcome the user to the web application. -->
    <div class="row">
        <div class="col-md-12">
            <h1>Welcome to Project Bazaar!</h1>
            <p class="lead">Hello and welcome to Project Bazaar! This is a web application that has been designed by a student for students to address the problem of finding information about what a final year project entails as well as being able to see suggestions made by members of staff and examples of these projects so that you can get a visual representation of exactly you will need to submit when it comes down to submitting your final year project. This was a problem that I myself encountered when starting my final year project not knowing where to look for information but also not finding the relevant information. I found it hard to understand what an acceptable project would be and only through the help of the university was I able to pick a suitable project.</p>
        </div>
    </div>
    <!-- Explain what a final year project entails. -->
    <div class="row">
        <div class="col-md-6">
            <h3>What is a final year project?</h3>
            <p class="lead">A project is large research and development project that is undertaken by third year computing students every year. The project must be large enough and challenging enough to meet the standard set by the university but also achievable within the set timeframe. These projects can takes months of careful planning and implementation to complete so it is important that the project you choose will keep you entertained and is generally something that you’re looking to go in to as a full time career as this project will provide vital experience of what it will be like to work in a real world environment if undertaken correctly. The project will include a deliverable artefact such as a piece of software or a web application as well as a written report to accompany the project showing the design and development from start to finish. There is a wide variety of projects that can be chosen specializing in many different areas so there will always be a project that you will find suited to yourself.</p>
        </div>
        <div class="col-md-6">       
            <img src="images/uni_grad.jpg" class="img-rounded projectExample">
            <img src="images/thinking.svg" class="projectExample">
        </div>
    </div>
</div>
@endsection
