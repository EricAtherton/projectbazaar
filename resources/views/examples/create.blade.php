 <!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Create new project example @endsection

@section('content')
<div class="container">

    <!-- If the user is a student then dont allow them to see the create project example form and return message to let them know they dont have access -->
	@if($user->accesslevel == 1)
		<p class="lead">You don't have access to this page.</p>
	@else
    <!-- Create a form for adding a new project example styled with Bootstrap CSS. Form wil post to the project examples controller where it will be handeled. --> 
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Create new project example:</div>
                <div class="panel-body">
                	{!! Form::open(array('route' => 'projectExamples.store', 'files' => true)) !!}
                		<div class="form-group">
                			{!! Form::label('grade', 'Grade achieved:', array('class' => 'formLabel')) !!}
                			{!! Form::select('grade', array('First' => 'First', '2:1' => '2:1', '2:2' => '2:2'), null, array('class' => 'form-control')) !!}
                		</div>
                		<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                			{!! Form::label('image', 'Add new image:', array('class' => 'formLabel')) !!}
                			{!! Form::file('image', array('class' => 'form-control')) !!}
                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group">
                			{!! Form::label('idea', 'Attach to project suggestion:', array('class' => 'formLabel')) !!}
                			{!! Form::select('idea', $ideas, null, array('class' => 'form-control')) !!}
                		</div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Create example', array('class' => 'btn btn-success btn-block')) !!}
                                <!-- Add a cancel button so the user can cancel the action if they want to and return them to the project examples overview page. -->
	                			<div class="cancelButton"><a href="{{ url('/projectExamples') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection