<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Project Examples @endsection

@section('content')

<div class="container">
	<!-- If a message is set display it to the user. -->
	@if(Session::get('message'))
	    <div class="alert alert-success">
	        <strong>Project example: </strong>{{ Session::get('message') }}
	    </div>
	@endif
	<!-- Introduce the user to the page. -->
	<div class="row">
	    <div class="col-md-12">
	        <h1>Project Examples</h1>
	        <p class="lead">Project examples are displayed below, these are all previous projects that students have submitted for their final year projects. The grade that they achieved for the submitted project is also included so that you can get a feel of the ammount of work you will need to undertake in order to be able to achieve your desired grade. There are various different examples for the various project ideas, to see more information about an example please click the project title which will take you to a more indepth view about that project.</p>
	    </div>  
	</div>
	<!-- Add a button for creating a project example if the user is staff or an admin. Add another button for all users to see all project suggestions. -->
	<div class="row">
		<div class="col-md-12">
			<p>
				@if($user->accesslevel != 1)
				<a href="{{ route('projectExamples.create') }}" class="btn btn-success"><i class="fa fa-btn fa-plus-circle"></i>Add new project example</a>
				@endif
				<a href="{{ url('/projectIdeas') }}" class="btn btn-primary">View all project suggestions</a>
			</p>
		</div>
	</div>
	<!-- Loop over all the project examples displaying them to the user with the project title to accompany it. Include the grade achieved for the example below the images. -->
	<div class="row">
		@foreach($examples as $example)
		    <div class="col-md-6">
		    	<h2><a href="{{ route('projectIdeas.show', [$example->idea->id]) }}" class="normalLinks">{{ $example->idea->title }}</a></h2>
		    	@if($user->accesslevel == 1)	
					
				@elseif($example->idea->owner == $user->id || $user->accesslevel == 3)
					<a href="{{ route('projectExamples.edit', [$example->id]) }}" class="btn btn-success"><i class="fa fa-btn fa-edit"></i>Edit</a></td>
					{!! Form::model($example, array('route' => array('projectExamples.destroy', $example->id), 'method' => 'delete', 'class' => 'deleteButton', 'onsubmit' => 'return ConfirmDelete()')) !!}
						{!! Form::hidden('_method', 'DELETE') !!}
                    	{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
					{!! Form::close() !!}
				@endif
		    	@foreach($example->images as $image)
		    		<img src="{{ $image->path }}" class="projectExample">
		    	@endforeach
		    	<p class="lead"><strong>Grade achieved for project example above: </strong>{{ $example->grade }}</p>
		    </div>
	    @endforeach
	</div>
</div>

@endsection