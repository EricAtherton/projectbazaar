<!-- Load in master layout -->
@extends('layouts.master')

<!-- Set page title -->
@section('title') Create new category @endsection

@section('content')
<div class="container">

    <!-- If the user is a student then dont allow them to see the create categorys form and return message to let them know they dont have access -->
	@if($user->accesslevel == 1)
		<p class="lead">You don't have access to this page.</p>
	@else 
    <!-- Create a form for adding a new category styled with Bootstrap CSS. Form wil post to the categorys controller where it will be handeled. -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Create new category:</div>
                <div class="panel-body">
                	{!! Form::open(array('url' => 'categorys')) !!}
                		<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                			{!! Form::label('category', 'Category name:', array('class' => 'formLabel')) !!}
                			{!! Form::text('category', null, array('class' => 'form-control')) !!}
                            @if ($errors->has('category'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </span>
                            @endif
                		</div>
                		<div class="form-group">
                			<div class="formButtons">
	                			{!! Form::submit('Add category', array('class' => 'btn btn-success btn-block')) !!}
                                <!-- Add a cancel button so the user can cancel the action if they want to and return them to the project ideas overview page. -->
	                			<div class="cancelButton"><a href="{{ url('/projectIdeas') }}" class="btn btn-danger btn-block">Cancel</a></div>
                			</div>
                		</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection