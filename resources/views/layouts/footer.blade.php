<footer class="footer">
    <div class="container">
        <!-- Add a signature and some quick links for users who are logged in. Depending on what user level they are will depend on what links they will see with admins having a quicklink to all the applications features. -->
    	<div class="col-md-6">
    		<p class="footerSignature">Designed and developed by Eric Atherton</p>
    		@if(Auth::guest())
    		@else
    		<h3 class="footerHeader">Quick Links</h3>
    		<ul class="list-unstyled">
    			<li><a href="{{ url('/home') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Home</a></li>
    			<li><a href="{{ url('/projectIdeas') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>All project ideas</a></li>
    			<li><a href="{{ url('/projectExamples') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>All project examples</a></li>
    			@if(Auth::user()->accesslevel != 1)
    				<li><a href="{{ url('/users') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>All users</a></li>
    			@else 
    				<li><a href="{{ url('/users') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Staff</a></li>
    			@endif
    			@if(Auth::user()->accesslevel == 3)
    				<li><a href="{{ url('/users/create') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Create new user</a></li>
    			@endif
    			@if(Auth::user()->accesslevel != 1)
    				<li><a href="{{ url('/projectIdeas/create') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Submit new project idea</a></li>
    				<li><a href="{{ url('/projectExamples/create') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Submit new project example</a></li>
                    <li><a href="{{ url('/categorys/create') }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Add new category</a></li>
    			@endif
    			<li><a href="{{ route('users.edit', [Auth::user()->id]) }}" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-right"></i>Edit account details</a></li>
    		</ul>
    	</div>
        <!-- Provide links that will stick to the top of the footer for accessing the top of the page and logging out. -->
       	<div class="col-md-6">
       		<ul class="footerButtonLinks">
            @if (Auth::user())
           		<li><a href="#" class="footerLinks"><i class="fa fa-btn fa-chevron-circle-up"></i>Return to the top</a></li>
                <li><a href="{{ url('/logout') }}" class="footerLinks"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
            @endif
        	</ul>
        	@endif
       	</div> 
    </div>
</footer>

<!-- JavaScripts - Load jquery and bootstrap JS files -->
<script src="{{ url('js/jquery-2.2.2.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/javascript.js') }}"></script>