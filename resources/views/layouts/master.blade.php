<!DOCTYPE html>
<!-- Produce basic HTML template which will be used by every page. -->
<html lang="en">

	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Get page title from individual pages. -->
    	<title>Project Bazaar - @yield('title')</title>

    	<!-- Fonts -->
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    	<!-- Styles -->
    	<link rel="stylesheet" href="{{ url('css/bootstrap.css' )}}" type="text/css"/>
        <link rel="stylesheet" href="{{ url('css/stylesheet.css' )}}" type="text/css"/>

	</head>

	<body id="app-layout">
        <!-- Load header layout -->
		@include('/layouts/header')
        <!-- Get content from individual pages. -->
    	@yield('content')
        <!-- Load footer layout -->
    	@include('/layouts/footer')
	</body>

</html>