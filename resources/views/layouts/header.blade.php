<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
       <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/home') }}">
                Project Bazaar
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li><a href="{{ url('/projectIdeas') }}">Project Ideas</a></li>
                <li><a href="{{ url('/projectExamples') }}">Project Examples</a></li>
                @if(Auth::guest())
                @else
                    <!-- Provide link based on users access level if they are a student then show staff, if they are staff or an admin show all users. -->
                    @if(Auth::user()->accesslevel != 1)
                        <li><a href="{{ url('/users') }}">All users</a></li>
                    @else
                        <li><a href="{{ url('/users') }}">Staff</a></li>
                    @endif
                @endif
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <!-- Create dropdown menu using jQuery. Use the logged in users name as the link -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <i class="fa fa-chevron-circle-down"></i>
                        </a>

                        <!-- Dropdown menu contains links for updating account and also logging out. -->
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('users.edit', [Auth::user()->id]) }}"><i class="fa fa-btn fa-edit"></i>Update</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                    <!-- Show users profile image. -->
                    <li><img src="{{ Auth::user()->profileimage }}" class="profileImage img-circle"></li>
                @endif
            </ul>
        </div>
    </div>
</nav>